export default function Navbar() {


  return (
    <>

      <nav className="navbar navbar-expand-lg bg-body-tertiary">
        <div className="container-fluid a">

          <a className="navbar-brand ms-5 me-5" href="/" ><img src="/img/Book.png" alt="logo" /></a>

          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">

              <li className="nav-item">
                <a href="/article/add" className="btn btn-outline-warning">Créer un article</a>

              </li>



            </ul>

          </div>
        </div>
      </nav>

    </>

  );
}
