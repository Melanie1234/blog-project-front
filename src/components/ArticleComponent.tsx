import { Article } from "@/entities";
import Link from "next/link";
import router from "next/router";

interface Props {
    article: Article;
}


export default function ArticleComponent({ article }: Props) {

    return (
        <div className="card w-75 d-flex m-auto mb-5 mt-5">
            <div className="row g-0">
                <div className="col-md-4">
                    <img src={article.img} className="img-fluid rounded-start" alt="..." />
                </div>
                <div className="col-md-8">
                    <div className="card-body">
                        <p className="card-text">{article.date}</p>
                        <h5 className="card-title">{article.title}</h5>
                        <p className="card-text">{article.author}</p>
                        <p className="card-text category">{article.category}</p>
                        <p className="card-text pseudo">{article.pseudo}</p>
                        
                        <Link href={"/article/" + article.id} className="card-link link">Lire plus
                        </Link>
                    </div>
                </div>
            </div>
        </div>

    );
}
