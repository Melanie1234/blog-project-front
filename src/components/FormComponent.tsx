import { postArticle } from "@/article-service"
import { Article } from "@/entities"
import { useRouter } from "next/router"
import { FormEvent, useState } from "react"

export const FormComponent = () =>{
    const router = useRouter()
    const [article, setArticle] = useState<Article>(

        {id:1, img:'', title: '', content: '', date: '', author: '', pseudo: '', category: ''}
    )

    function handleChange (event: any){
        setArticle({
            ...article,
        [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event: FormEvent){
        event.preventDefault();
        const added = await postArticle(article)
        router.push('/article/'+added.id)

    }

    return(

        <form onSubmit={handleSubmit} className="card w-75 m-auto p-5 mt-5 mb-5">
            <h3>Ajouter un article</h3>

                <div className="mb-3">
                    <label className="form-label"> Image </label>
                    <input type="text" value={article.img} className="form-control" name="img" onChange={handleChange} />
                </div>

                <div className="mb-3">
                    <label className="form-label"> Titre </label>
                    <input type="text" value={article.title} className="form-control" name="title" onChange={handleChange} />
                </div>

                <div className="mb-3">
                    <label className="form-label">Auteur</label>
                    <input type="text" value={article.author} className="form-control" name="author" onChange={handleChange} aria-label="Auteur" />
                </div>
                <div className="mb-3">
                    <label className="form-label">Genre</label>
                    <input type="text" value={article.category} className="form-control" name="category" onChange={handleChange}  aria-label="category" />
                </div>

                <div className="mb-3">
                    <label className="form-label">Date</label>
                    <input type="date" value={article.date} className="form-control" name="date" onChange={handleChange} aria-label="date" />
                </div>

                <div className="mb-3">
                    <label className="form-label">Pseudo</label>
                    <input type="text" value={article.pseudo} className="form-control" name="pseudo" onChange={handleChange}  aria-label="pseudo" />
                </div>
                
                <div className="mb-3">
                    <label className="form-label">Texte</label>
                    <textarea value={article.content} className="form-control" name="content" onChange={handleChange} aria-label="content" />
                </div>

               

            
            <button className="btn btn-outline-success mt-5">Ajouter l'article</button>
        </form>


    )


}


