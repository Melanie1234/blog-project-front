export default function footerComponent() {

    return (
        <nav className="navbar bg-light ">
                <span className="navbar-text footer m-auto">
                <a className="navbar-brand" href="/" ><img src="/img/Book.png" alt="logo" /></a>

                </span>
        </nav>

    )
}