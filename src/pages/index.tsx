import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";

import { Article } from "@/entities";
import { useEffect, useState } from "react";
import { fetchAllArticles } from "@/article-service";
import ArticleComponent from "@/components/ArticleComponent";





export default function Index() {


    const [articles, setArticles] = useState<Article[]>([]);

    useEffect(() => {
        fetchAllArticles().then(data => {
            setArticles(data);
        })
    })
   
    return (
        <div>
            {articles.map(item =>
                <ArticleComponent key={item.id} article={item} />
            )}
             
        </div>
    );

}
