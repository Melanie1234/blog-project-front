import { deleteArticle, fetchOneArticle } from "@/article-service";
import { Article } from "@/entities";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export default function ArticlePage() {
    const router = useRouter();
    const { id } = router.query;
    const [article, setArticle] = useState<Article>();

    useEffect(() => {
        (async () => {
            if (!id) {
                return;
            }
            const data = await fetchOneArticle(Number(id));
            setArticle(data);

        })();
    }
    , [id])

    async function remove() {
        await deleteArticle(id);
        router.push('/');
    }


    if (!article) {
        return <p>Loading...</p>
    }

    return (
        
    <div className="card w-75 d-flex m-auto mb-5 mt-5 p-5">  
    <div className="card-body">
    <h2 className="card-title">{article.title}</h2>
    <h6 className="card-subtitle mb-2 text-muted">{article.author}</h6>
    <br />
    <p className="card-text category">{article.category}</p>
    <p className="card-text content">{article.content}</p>
    <p className="card-text">{article.date}</p>
    <p className="card-text pseudo">{article.pseudo}</p>


    <button onClick={remove} className="btn btn-outline-info">Effacer l'article</button>


  </div>
</div>          

);
}
