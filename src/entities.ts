export interface Article {
    id?:number;
    img:string;
    title:string;
    content:string;
    date:string;
    author:string;
    pseudo: string;
    category:string

}
