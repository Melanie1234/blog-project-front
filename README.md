## Name
Blog

## Description
Ce projet se compose de 2 parties, une front et une back.
Concernant la partie front, l'objectif est d'afficher les articles du blog et de proposer des options telles que la création d'un article via un formulaire ou la suppression d'un article. 
Pour ce faire, j'ai tout d'abord crée les composants principaux de mon Blog (header, formulaire, article et footer). Par la suite, j'ai organisé la page d'accueil avec ces différents composants. Enfin, j'ai lié mes composants avec les données présentent dans la partie back du projet.

## Built With
Ce projet a été réalisé en React avec Next.js et la mise en forme via Bootstrap.

La maquette est consultable sur https://www.figma.com/file/JP7APWTdZRra8H3AK9R1zN/Blog?node-id=0%3A1&t=SDjuGy1nUAAWET2q-0

## Roadmap
Des améliorations sont à prévoir, principalement sur l'aspect optimisation du code. 

## Contact
Pour toutes interrogations ou propositions d'améliorations, vous pouvez me contacter à l'adresse melanie.ferer1@gmail.com